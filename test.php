<?php 
require_once('wp-load.php');

function get_coordinates() {
    // $origin = rawurlencode('1st Megasaver, MacArthur Highway, Tarlac City, Tarlac');
    $destination = urlencode('Baras-Baras, Tarlac City, Tarlac');

    $gapi_key = 'AIzaSyDLLxP4n2__hjKK09m7d1aL5kjdKWiXOSg'; 
    $shippingurl = "https://maps.googleapis.com/maps/api/geocode/json?address=$destination&key=$gapi_key";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$shippingurl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // Disable SSL verification
    $response = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($response);

    if ($response->status == 'ZERO_RESULTS') {
        return false;
    }
    else {
        $return = array('lat' => $response->results[0]->geometry->location->lat, 'lng' => $response->results[0]->geometry->location->lng);
        return $return;
    }
}

function get_distance_matrix($destination_loc,$nearest_store) {
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$nearest_store[0].",".$nearest_store[1]."&destinations=".$destination_loc['lat'].",".$destination_loc['lng']."&mode=driving&language=ph-PH&key=AIzaSyCVqlS5AGgJ3G9lCA32WmtGeL5bHN6JtZQ";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

    return array('distance' => $dist, 'time' => $time);  
    
}

function get_nearest_store($destination_loc) {
    $locations = fetch_branches();
    // calculate distances from customer to store
    foreach ($locations as $key => $location) {
        $loc = explode(',',$location->lat_long);
        $a = $destination_loc['lat'] - $loc[0];
        $b = $destination_loc['lng'] - $loc[1];
        $distance = sqrt(($a**2)+($b**2)); // phytagorean theorem
        $distances[$key] = $distance;
    }

    asort($distances);
    $dists = array();
    foreach ($distances as $key => $val) {
        $dists[$key] = $val;
    }

    return $locations[key($distances)];    
}

function fetch_branches() {
    global $wpdb;

    $locations = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}branches WHERE lat_long IS NOT NULL");
    return $locations;
}

$destination_loc = get_coordinates();
if (!$destination_loc) {
    echo "Please check your shipping address";
    return;
}
$nearest_store = get_nearest_store($destination_loc);
echo json_encode(array(
    'customer' => $destination_loc,
    'store' => $nearest_store,
    get_distance_matrix($destination_loc,explode(',', $nearest_store->lat_long))
    )
);

?>