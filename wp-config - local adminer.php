<?php

// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';


// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';

define('WP_CACHE', true);

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://codex.wordpress.org/Editing_wp-config.php

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', 'local' );


/** MySQL database username */

define( 'DB_USER', 'root' );


/** MySQL database password */

define( 'DB_PASSWORD', "root" );


/** MySQL hostname */

define( 'DB_HOST', 'localhost' );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8mb4' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         'v*s>B!OeTwD*YXd$@td4HNLaTWu>h@HI/}77|NJ%3t7sfbG;_uTT&xZCA%L9yni6' );

define( 'SECURE_AUTH_KEY',  'qN2txMhSH=FE/tLk80tf<*xLO8:;,xsaeFYeQBYVs<0]i:DOWG/c}%4HyQ)wh6E9' );

define( 'LOGGED_IN_KEY',    'b{eVvGT-82p:R_W]@VH/aQ4zt}zi]M)tDlNclufD0sb@&$0I&RV`= b |xgx:2xo' );

define( 'NONCE_KEY',        '/!oRoMYpD8xw$As8jn<$1xG1J`7s05Y2DK>S?KB]a=^~B4ZU<i014?cW=GK@avrt' );

define( 'AUTH_SALT',        'c.r{In<X3XJm~E.A!A]@3Pi4Ql3T68!ikY<~57~?Y::5j11J6su@oI=844?_3:xX' );

define( 'SECURE_AUTH_SALT', 'EV`Q8EHS?5,oK@IrG#neKd-.mg*8a~mVdgMy.$e71nZ7j9em$;C^<qaUWJiL5NQ!' );

define( 'LOGGED_IN_SALT',   'w*SbX@sY=sZ0I,x4/~9RsNCUlKbw2M}r#}1#cY<cCkg|Xdlk6S2%HR<~[ab?AVu ' );

define( 'NONCE_SALT',       'Ca.;TVlEY9WvYi,Q`^gc/VI0_>D$uZ<t}kv1t^{u8@44QX6V2TK)@(?H:F[CEJ+=' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the Codex.

 *

 * @link https://codex.wordpress.org/Debugging_in_WordPress

 */

// define( 'WP_DEBUG', true );
// define( 'WP_DEBUG_LOG', true );

define('WP_MEMORY_LIMIT', '256M');
define( 'WP_MAX_MEMORY_LIMIT', '256M' );

define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 0 );
// define('SAVEQUERIES',true);

// // log php errors
// @ini_set('log_errors','On'); // enable or disable php error logging (use 'On' or 'Off')
// @ini_set('error_log','C:/xampp/php/logs/php-errors.log'); // path to server-writable log file
//define('VP_ENVIRONMENT', 'development-gio');
/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

}


/** Sets up WordPress vars and included files. */

require_once( ABSPATH . 'wp-settings.php' );

