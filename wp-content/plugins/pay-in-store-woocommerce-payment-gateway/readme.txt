=== Pay in Store WooCommerce Payment Gateway ===
Contributors: enartia,g.georgopoulos,georgekapsalakis
Author URI: https://www.enartia.com
Tags: ecommerce, woocommerce, payment gateway
Tested up to: 5.1.0
Requires at least: 4.0
Version: 1.1
WC tested up to: 3.5.5
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Provides a Pay in Store upon pick up Payment Gateway for Woocommerce.
== Description ==
Provides a Pay in Store upon pick up Payment Gateway for Woocommerce.
== Changelog ==
= 1.0.4 =
WooCommerce 3.0 compatible
= Version: 1.0.0 =
Initial Release