<?php if( $title ) echo $args['before_title'].$title.$args['after_title'];
$args = array(
    'hide_empty' => ( @ $hide_empty ? true : false ),
);
$BeRocket_product_brand = BeRocket_product_brand::getInstance();
$options = $BeRocket_product_brand->get_option();
if( ! empty( $count ) ) {
    $args['number'] = $count;
}
if( ! empty( $orderby ) ) {
    $args['orderby'] = $orderby;
}
$terms = get_terms( 'berocket_brand', $args );
if( ! empty($terms) && is_array($terms) && count($terms) > 0 ) {
    $width = 100 / (int)$per_row;
    global $berocket_unique_value;
    $berocket_unique_value++;
    $slider_rand = $berocket_unique_value;
    if( ! empty($slider) ) {
        wp_enqueue_style( 'berocket_slick_slider' );
    ?>
    <script>
    (function ($){
        $(document).ready( function () {
            <?php 
            if( empty($options['slider_infinite']) ) {
                echo "$('.brcs_slider_brands:not(\".slick-slider\")').on('afterChange init', function(event, slick) {
                    $(this).find('.slick-prev').show();
                    $(this).find('.slick-next').show();
                    if (slick.currentSlide === 0) {
                        $(this).find('.slick-prev').hide();
                    } else if($(this).find('.slick-slide').last().is('.slick-active')) {
                        $(this).find('.slick-next').hide();
                    }
                });";
            }
            ?>
            $('.brcs_slider_brands:not(".slick-slider")').slick({
                prevArrow:'<button type="button" class="slick-prev"><i class="fa fa-chevron-left"></i></button>',
                nextArrow:'<button type="button" class="slick-next"><i class="fa fa-chevron-right"></i></button>',
                autoplay:<?php echo ( empty($options['slider_autoplay']) ? 'false' : 'true' ); ?>,
                autoplaySpeed:<?php echo ( empty($options['slider_autoplay_speed']) ? '5000' : $options['slider_autoplay_speed'] ); ?>,
                infinite:<?php echo ( empty($options['slider_infinite']) ? 'false' : 'true' ); ?>,
                arrows:<?php echo ( empty($options['slider_arrows']) ? 'false' : 'true' ); ?>,
                pauseOnFocus:<?php echo ( empty($options['slider_stop_focus']) ? 'false' : 'true' ); ?>,
            });
        });
    })(jQuery);
    </script>
    <?php
        wp_enqueue_script( 'berocket_slick_slider_js');
        wp_enqueue_style( 'berocket_slick_slider' );
        wp_enqueue_style( 'font-awesome');
        $slider_col = $per_row;
        $loop = new WP_Query( $args );
        if( empty($slider_col) ) {
            $slider_col = 3;
        }
        echo '<div data-slick=\'{"slidesToShow":'.$slider_col.',"slidesToScroll":' . ( empty($options['slider_slides_scroll']) ? $slider_col : $options['slider_slides_scroll'] ) . '}\' class="brcs_slider_brands br_brand_', $slider_rand, '">';
        foreach($terms as $term) {
            echo '<div class="br_widget_brand_element_slider">';
            if( $use_image ) {
                $image 	= get_term_meta( $term->term_id, 'brand_image_url', true );
                if( ! empty($image) ) {
                    echo '<div class="brand_slider_image"><a href="', get_term_link( $term->slug, 'berocket_brand' ), '"><img src="', $image, '" alt="', $term->name, '"></a></div>';
                }
            }
            if( $use_name ) {
                echo '<a href="', get_term_link( $term->slug, 'berocket_brand' ), '">', $term->name, '</a>';
            }
            echo '</div>';
        }
        echo '</div>';
        if( substr($border_color, 0, 1) != '#' ) {
            $border_color = '#'.$border_color;
        }
        echo '<style>.br_brand_', $slider_rand, ' .br_widget_brand_element_slider{
            padding:', $padding, ';
            box-sizing: border-box;
            ', ( ( ! empty ($border_color) && ! empty ($border_width) ) ? 'border: ' . $border_width . 'px solid ' . $border_color : '' ), '!important;
        }';
        if( ! empty($imgh) ) {
            echo '.br_brand_', $slider_rand, ' .br_widget_brand_element_slider .brand_slider_image{
                height: ', $imgh, 'px;
                line-height: ', $imgh, 'px;
            }';
            echo '.br_brand_', $slider_rand, ' .br_widget_brand_element_slider .brand_slider_image img{
                max-height: ', $imgh, 'px;
            }';
        }
        echo '</style>';
    } else {
        echo '<div class=" br_brand_', $slider_rand, '">';
        foreach($terms as $term) {
            echo '<div class="br_widget_brand_element" style="width:', ($width - 1), '%;float:left;margin-right: 1%;">';
            if( $use_image ) {
                $image 	= get_term_meta( $term->term_id, 'brand_image_url', true );
                if( ! empty($image) ) {
                    echo '<a class="brand_image_link" href="', get_term_link( $term->slug, 'berocket_brand' ), '"><img src="', $image, '" alt="', $term->name, '"', ( empty($imgh) ? '' : ' style="max-height:'.$imgh.'px;"' ), '></a>';
                }
            }
            if( $use_name ) {
                echo '<a href="', get_term_link( $term->slug, 'berocket_brand' ), '">', $term->name, '</a>';
            }
            echo '</div>';
        }
        echo '</div>';
        if( substr($border_color, 0, 1) != '#' ) {
            $border_color = '#'.$border_color;
        }
        echo '<style>.br_brand_', $slider_rand, ' .br_widget_brand_element{
            padding:', $padding, ';
            box-sizing: border-box;
            ', ( ( ! empty ($border_color) && ! empty ($border_width) ) ? 'border: ' . $border_width . 'px solid ' . $border_color : '' ), '!important;
        }';
        if( ! empty($imgh) ) {
            echo '.br_brand_', $slider_rand, ' .brand_image_link{
                display: inline-block;
                height: ', $imgh, 'px;';
        }
        echo '</style>';
        echo '<div style="clear:both;"></div>';
    }
}
?>
