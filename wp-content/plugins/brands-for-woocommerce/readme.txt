=== Brands for WooCommerce ===
Plugin Name: Brands for WooCommerce
Contributors: dholovnia, berocket
Donate link: https://berocket.com/?utm_source=wordpress_org&utm_medium=donate&utm_campaign=product_brand
Tags: brands, manufacturer, supplier, product brand, brands for product, woocommerce brands, woocommerce manufaturer, woocommerce supplier, wc brands, wc product brands, wc showcase brands, brand page, widget, plugin, brands plugin, brands widget, brands slider, slider with brands, showcase brands, page for brands, brands page plugin, brands images show, berocket, berocket brands for woocommerce
Requires at least: 4.0
Tested up to: 5.2.2
Stable tag: 3.5.0.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Brands for WooCommerce plugin allows you to create brands for products on your shop.

== Description ==

Brands for WooCommerce plugin allows you to create brands for products on your shop. Each brands has name, description and image.

= Features: =
&#9989; Custom image for brands
&#9989; Custom pages for brands
&#9989; Shortcode to display products by brand ID
&#9989; Widget with links for brand pages
&#9989; Display brand description and image on brand pages


= Features in Version 3.5: =
&#9989; Slider for brand links
&#9989; Additional customization
&#9989; Widget and shortcode for product brands list by name
&#9989; Shotcode to display brand image on product page
&#9989; Shortcode and widget to display brand information
&#9989; Option to display brand image on product pages


= Plugin Links: =
[Demo](https://woocommerce-brands.berocket.com/shop/?utm_source=wordpress_org&utm_medium=plugin_links&utm_campaign=product_brand)
[Demo Description](https://woocommerce-brands.berocket.com?utm_source=wordpress_org&utm_medium=plugin_links&utm_campaign=product_brand)


= &#127852; Wanna try admin side? =
[Admin Demo](https://berocket.com/product/woocommerce-brands?utm_source=wordpress_org&utm_medium=admin_demo&utm_campaign=product_brand#try-admin) - Get access to this plugin's admin and try it from inside. Change things and watch how they work.

= Premium plugin video =
[youtube https://youtu.be/lATsHNMWpgM]
*we don't have video with free plugin right now but we are working on it*

= Compatibility with WooCommerce plugins =
Brands for WooCommerce has been tested and compatibility is certain with the following WooCommerce plugins that you can add to your site:

&#128312; [**Advanced AJAX Product Filters**](https://wordpress.org/plugins/woocommerce-ajax-filters/)
&#128312; [**Force Sell for WooCommerce**](https://wordpress.org/plugins/force-sell-for-woocommerce/)
&#128312; [**Min and Max Quantity for WooCommerce**](https://wordpress.org/plugins/minmax-quantity-for-woocommerce/)
&#128312; [**Product of the Day for WooCommerce**](https://wordpress.org/plugins/product-of-the-day-for-woocommerce/)
&#128312; [**Products Compare for WooCommerce**](https://wordpress.org/plugins/products-compare-for-woocommerce/)
&#128312; [**Product Tabs Manager for WooCommerce**](https://wordpress.org/plugins/product-tabs-manager-for-woocommerce/)

== Installation ==
Important: First of all, you have to download and activate WooCommerce plugin, because without it Brands for WooCommerce cannot work.

1. Unzip the downloaded .zip file.
2. Upload the Brands for WooCommerce folder into the `wp-content/plugins/` directory of your WordPress site.
3. Activate `Brands for WooCommerce` from Plugins page

= Configuration =
Brands for WooCommerce will add a new sub-menu item called "Brands" in "WooCommerce" menu. There you will be able to access plugin settings page.


== Frequently Asked Questions ==

= Is it compatible with all WordPress themes? =
Compatibility with all themes is impossible, because they are too many, but generally if themes are developed according to WordPress and WooCommerce guidelines, BeRocket plugins are compatible with them.

= How can I get support if my WooCommerce plugin is not working? =
If you have problems with our plugins or something is not working as it should, first follow this preliminary steps:

* Test the plugin with a WordPress default theme, to be sure that the error is not caused by the theme you are currently using.
* Deactivate all plugins you are using and check if the problem is still occurring.
* Ensure that your plugin version, your theme version and your WordPress and WooCommerce version (if required) are updated and that the problem you are experiencing has not already been solved in a later plugin update.

If none of the previous listed actions helps you solve the problem, then, submit a ticket in the forum and describe your problem accurately, specify WordPress and WooCommerce versions you are using and any other information that might help us solve your problem as quickly as possible. Thanks!

---

== Screenshots ==
1. General settings
2. Brands creation

---

== Changelog ==

= 3.5.0.2 =
* Fix - Links to BeRocket
* Fix - Compatibility with other BeRocket plugins

= 3.5.0.1 =
* Fix - Some notices displayed incorrect
* Fix - Compatibility with other plugins

= 3.5 =
* Enhancement - Slider for brand links
* Enhancement - Additional customization
* Enhancement - Widget and shortcode for product brands list by name
* Enhancement - Shotcode to display brand image on product page
* Enhancement - Shortcode and widget to display brand information
* Enhancement - Option to display brand image on product pages

= 1.1.0.4 =
* Enhancement - Compatibility with other BeRocket plugins

= 1.1.0.3 =
* Enhancement - Code Security

= 1.1.0.2 =
* Enhancement - Code Security

= 1.1.0.1 =
* Enhancement - Brands in navigation menu
* Fix - Prepare for WooCommerce 3.6.0

= 1.1 =
* Enhancement - New settings design
* Enhancement - New slider for brands

= 1.0.12.1 =
* Fix - Compatibility with AJAX Filters

= 1.0.12 =
* Fix - Subscribe
* Fix - Feature request send

= 1.0.11 =
* Enhancement - Feature request box
* Enhancement - Feedback box

= 1.0.10 =
* Upgrade - better plugin menu items location

= 1.0.9 =
* Upgrade - WordPress 4.9 compatibility

= 1.0.8 =
* Upgrade - more useful subscribe
* Fix - updater fix

= 1.0.7 =
* Upgrade - recent woocommerce version support
* Upgrade - new Admin notices

= 1.0.6 =
* Upgrade - Option to subscribe
* Upgrade - Better advertisement

= 1.0.5 =
* Fix - Description
* Fix - Premium plugin link on settings page

= 1.0.4 =
* Compatibility with WooCommerce 3.0.0
* Widgets fix
* Better compatibility with WPML

= 1.0.3 =
* Small fixes

= 1.0.2 =
* Better support for PHP 5.2

= 1.0.1 =
* First public version
