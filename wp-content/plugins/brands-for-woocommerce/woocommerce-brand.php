<?php
/**
 * Plugin Name: Brands for WooCommerce
 * Plugin URI: https://wordpress.org/plugins/brands-for-woocommerce/?utm_source=free_plugin&utm_medium=plugins&utm_campaign=product_brand
 * Description: Brands are a guarantee for quality, they assure product recognition in customers. WooCommerce Product Brands will help you to showcase them.
 * Version: 3.5.0.2
 * Author: BeRocket
 * Requires at least: 4.0
 * Author URI: https://berocket.com?utm_source=free_plugin&utm_medium=plugins&utm_campaign=product_brand
 * Text Domain: brands-for-woocommerce
 * Domain Path: /languages/
 * WC tested up to: 3.6.4
 */
define( "BeRocket_product_brand_version", '3.5.0.2' );
define( "BeRocket_product_brand_file", __FILE__ );
include_once('main.php');
