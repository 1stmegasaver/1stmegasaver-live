jQuery(function($){

  $('.woocommerce .woocommerce-checkout .woocommerce-billing-fields h3')
    .empty()
    .html('<i class="fas fa-map-marker-alt"></i> SHIPPING DETAILS');
  $('.woocommerce-checkout h3#order_review_heading').prepend('<i class="fas fa-file-alt"></i> ');

  // get all city_municipalities
  var cities_json = null;
  var pre_select_state = null;
  var pre_select_city = null;
  $.getJSON(cust_global.templateUrl+"/wp-content/themes/1stmegasaver/json/ph_citymun.json" )
      .done(function( json ) {
        cities_json = json;
        pre_select_state = $('#billing_state option:selected').html();
        provcode = prov_code(pre_select_state);
          if (pre_select_state != 'Select an option...') 
	          {
	          	filter_cities(provcode);
	          	$('#billing_postcode').val('');
	          }    
      })
      .fail(function( jqxhr, textStatus, error ) {
        var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
  });


  pre_select_city = $('#billing_city option:selected').html();
  if (pre_select_city == 'Select an option...') 
  {
    $('#billing_postcode').val('');
  }    
     
  // init load cities
  function filter_cities(provcode) {
    // filter certain province
    var cities = $.grep(cities_json, function (n) { return n.provCode == provcode; },false);
    var JSONObject = JSON.parse(JSON.stringify(cities));

    newOptionsSelect = '';
    for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
            city = camelSentence(JSONObject[key]["citymunDesc"]);
            zip = JSONObject[key]["zipcode"];
            citymunCode = JSONObject[key]["citymunCode"];
            var newOptionsSelect = newOptionsSelect + '<option value="'+city+'" data-zipcode="'+zip+'" data-citycode="'+citymunCode+'">'+city+'</option>';
        }
    }

    $('#billing_city').find('option').remove()
      .end().append('<option selected="true">Select an option...</option>');
    $('#billing_city').append( newOptionsSelect );

    if (pre_select_state != 'Select an option...') {
      // $("#billing_city option[value=" + pre_select_state + "]").prop("selected",true);
      $('#billing_postcode').val($('#billing_city option:selected').attr('data-zipcode'));  
    } else {
       $('#billing_postcode').val('');
    }
  }

  $('#billing_state').on('change',function() {
    billing_state = $('#billing_state option:selected').html();
    pre_select_state = billing_state;
    provcode = prov_code(billing_state);
    filter_cities(provcode);
    $('#billing_postcode').val('');     
  });

   $('#billing_city').on('change',function() {
    billing_city = $('#billing_city option:selected').html();
    zip_code = $('#billing_city option:selected').attr('data-zipcode');
    // $('#billing_postcode').attr('disabled',true);
     $('#billing_postcode').val(zip_code);
  });

  function prov_code(prov) {
    // console.log(prov);
    var prov_codes = [
      {'provCode':'0308', 'provDesc':'BATAAN'},
      {'provCode':'0369', 'provDesc':'TARLAC'},
      {'provCode':'1411', 'provDesc':'BENGUET'},
      {'provCode':'0314', 'provDesc':'BULACAN'},
      {'provCode':'0215', 'provDesc':'CAGAYAN'},
      {'provCode':'0129', 'provDesc':'ILOCOS SUR'},
      {'provCode':'0231', 'provDesc':'ISABELA'},
      {'provCode':'0133', 'provDesc':'LA UNION'},
      {'provCode':'1339', 'provDesc':'MANILA'},
      {'provCode':'0349', 'provDesc':'NUEVA ECIJA'},
      {'provCode':'0250', 'provDesc':'NUEVA VIZCAYA'},
      {'provCode':'0354', 'provDesc':'PAMPANGA'},
      {'provCode':'0155', 'provDesc':'PANGASINAN'},
      {'provCode':'0257', 'provDesc':'QUIRINO'},
    ];

    for (var i = 0; i < prov_codes.length; i++) {
      if (prov_codes[i].provDesc == prov.toUpperCase()) 
        return prov_codes[i].provCode;
    }
  }

    function camelSentence(str) {
      if ((str===null) || (str===''))
       return false;
      else
       str = str.toString();

     return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  }   


});