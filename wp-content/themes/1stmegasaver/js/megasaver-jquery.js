	var $j=jQuery.noConflict();
    $j(document).ready(function() {

    $j('#menu-item-2267 a.menu-link span.menu-text').html('Hi,' + cust_global.theUser.firstname);

     $j('.share-icon').click(function(){
     	ops_onClick($j(this).attr("href"));
     });

    function ops_onClick(a){
  		var b="640",c="480",d=screen.height/2-c/2,e=screen.width/2-b/2,f="toolbar=0,status=0,width="+b+",height="+c+",top="+d+",left="+e;
  		open(a,"",f)
  	}

    $j(".toggle-password").click(function() {

      $j(this).toggleClass("fa-eye fa-eye-slash");
      var input = $j($j(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });

    $j("#searchform").on('submit',function(){return false;});
    $j("#searchform .search_branch_bt").on('click',function(e) {
       var query = $j.trim($j(this).prevAll('.search_query').val()).toLowerCase();
       
       if (query.indexOf("tan") != -1) {
         if (query.indexOf("ftan") != -1) {
          query = [query.slice(0, 1), '.', query.slice(1)].join('');
        }
         query = query.replace("n","ñ");
         console.log(query);
       }

       $j('div.store-container .branch-name').each(function(){
        var $this = $j(this);
           if($this.text().toLowerCase().indexOf(query) === -1) {
               $this.closest('div.store-container').fadeOut();
           }    
          else {$this.closest('div.store-container').fadeIn();}
      });
    });

    $j("#searchform .search_query").on('keyup',function(e) {
      if ($j(this).val().length == 0) {
        $j('.clear_search').css('display','none');
      } else {
        if ($j(this).val().length > 2) {
          $j(".search_branch_bt").click();
        }
        $j('.clear_search').css('display','inline-block');
      }
    });  

    $j("#searchform .clear_search").on('click',function(e) {
       e.preventDefault();
       $j(".search_query").val("");
       $j(".search_branch_bt").click();
       $j('.clear_search').css('display','none');
    });  

    $j('.search_query').keydown(function(e){
      if(e.keyCode == 8 && $j(this).val() == ''){
        $j(".search_branch_bt").click();
      }
    });
    
    $j('.elementor-1804 .elementor-icon-list-icon .fa-phone').addClass('fa-flip-horizontal');

    $j('.customer_service a').on('click',function(){
      Tawk_API.toggle();
    });

     $j('#chat_with_us').on('click',function(){
      Tawk_API.toggle();
    });

  resize_footer_copyright();   
  $j('.elementor-page-6 .eael-product-grid .woocommerce ul.products.eael-product-columns-6').css('width','calc('+screen.width+'px - 5%)');
  var WindowsSize=function(){
     var w=$j(window).width();
     $j('.elementor-page-6 .eael-product-grid .woocommerce ul.products.eael-product-columns-6').css('width','calc('+w+'px - 5%)');

     // remove element/s
     if (w <= 425) {
      $j('#mc4wp_form_widget-3').hide();
      console.log('removed widget');
     } else {
      $j('#mc4wp_form_widget-3').show();
     }

     resize_footer_copyright();
  };
   $j(window).resize(WindowsSize); 

     console.log($j(window).width());
     // console.log(screen.width);
     // alert("device width: " + $j(window).width());

  function resize_footer_copyright() {
    var demo_store = $j('p.demo_store').height();
    $j('#footer-bottom').css('margin-bottom',demo_store+25+'px');
    console.log('demo_store:'+demo_store);
  }

   // change hostname to localhost
  if(location.hostname=='localhost') {
    $j('a[href^="http://192.168.2.230:8080"]').each(function(index, element) {
      var link = $j(element).attr('href');
      var s=link.replace("192.168.2.230:8080","localhost");
      $j(element).attr('href',s);
    });
    
  }

  // change admin color
  if (/clone/.test(window.location.href)) {
    console.log('clone');
    $j('#wpadminbar').attr('style', 'background-color: purple !important');
    $j('.sticky').attr('style', 'top: 32px !important');
  }  

  $j("#mega-menu-wrap-sticky-menus").detach().insertAfter($j("#search-header-wrapper"));

  $j('.tinvwl_add_to_wishlist_button').attr('title','Add to your Wishlist');

  $j("#mega-menu-item-5246 a.mega-menu-link")
    .prepend("<img src='"+cust_global.templateUrl+"/wp-content/uploads/cats icons menus/gadgets.jpg'>");
  $j("#mega-menu-item-5242 a.mega-menu-link")
    .prepend("<img src='"+cust_global.templateUrl+"/wp-content/uploads/cats icons menus/home-enter-sounds.jpg'>");

    $j('section#my-brands').css('background-image',cust_global.templateUrl+'/wp-content/uploads/2019/06/hd1.jpg');

 
    $j('img').attr('draggable',false);

    $j('a.woocommerce-store-notice__dismiss-link').remove();


    //  ORDER SYSTEM SHOW/HIDE

    if (cust_global.theUser.role !== 'administrator') {
      // mini-cart icon in main menu
      //$j("#mega-menu-wrap-main_menu #mega-menu-main_menu .woo-menu-icon").css('display','none');
    
      // hide stock status column
      $j('.tinv-wishlist table .product-stock').attr('style', 'display: none !important');
      $j('.tinv-wishlist table .product-action').attr('style', 'display: none !important');
      $j(".tinv-wishlist table select option[value='add_selected']").remove();
      $j('.tinv-wishlist tfoot .tinvwl-to-right').attr('style', 'display: none !important');
    } else {
      $j('.tinv-wishlist table .product-stock').attr('style', 'display: table-cell !important');
      $j('.tinv-wishlist table .product-action').attr('style', 'display: table-cell !important');
      $j('.tinv-wishlist tfoot .tinvwl-to-right').attr('style', 'display: block !important');
    }

    if (!$j('body').hasClass('single-product')) {
       $j("#breadcrumbs").attr('style', 'display: none !important');
    } else {
      $j("#breadcrumbs").attr('style', 'display: block !important');
    }

    $j('.woocommerce-notices-wrapper ul li:before').click(function(){
      $j(this).parents('li').remove();
    });

    $j('span.tinvwl_add_to_wishlist-text').parents('a').addClass('add_to_wish_list_single_product');
});


$j(document).contextmenu(function() {

    var clone = false;
   if (/clone/.test(window.location.href)) {
    clone = true;
    }   
  console.log(clone);  
  console.log(cust_global.theUser.role);
  if(cust_global.theUser.role !== 'administrator')
    $return = false;
  else
    $return =true;

  if (clone)
    $return =true;

  return $return;
});