document.addEventListener('DOMContentLoaded', function() {
	
	var stickymenu = document.getElementById("search-header");//static-menu
	var stickymenuoffset = stickymenu.offsetTop;
	
	var cust_logo = document.getElementById("logo-mobile");
	var search_header = document.getElementById("search-header-wrapper");
	var search_tag_cloud = document.getElementById("search-tag-cloud");
	var sticky_categories = document.getElementById("mega-menu-wrap-sticky-menus");

	window.addEventListener("scroll", function(e){
		requestAnimationFrame(function(){
			if (window.pageYOffset > stickymenuoffset){
				stickymenu.classList.add('sticky');
				cust_logo.classList.add('sticky-logo-image');
				search_header.classList.add('sticky-search');
				search_tag_cloud.classList.add('sticky-search-suggestions');
				document.getElementsByClassName("elementor-element-0180b77")[0].style.display = "none";
				document.getElementsByClassName("elementor-element-904fa40")[0].style.display = "none";
				document.getElementsByClassName("elementor-element-2b2426f")[0].style.display = "none";
				// sticky_categories.classList.add('sticky-cats');
			}
			else{
				stickymenu.classList.remove('sticky');
				cust_logo.classList.remove('sticky-logo-image');
				search_header.classList.remove('sticky-search');
				search_tag_cloud.classList.remove('sticky-search-suggestions');
				// sticky_categories.classList.remove('sticky-cats');
				document.getElementsByClassName("elementor-element-0180b77")[0].style.display = "flex";
				document.getElementsByClassName("elementor-element-904fa40")[0].style.display = "flex";
				document.getElementsByClassName("elementor-element-2b2426f")[0].style.display = "flex";
			}
		})
	});


	if(cust_global.theUser.role !== 'administrator') {
		var body = document.body;
		body.classList.add("user_only_css");
	}
});