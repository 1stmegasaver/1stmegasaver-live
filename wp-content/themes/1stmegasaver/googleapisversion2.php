<?php 
define( 'WP_USE_THEMES', false );
require(dirname(__FILE__) . '/../../../wp-load.php');

function fetch_branches() {
    $strJsonFileContents = file_get_contents("json/megasaver_branches.json");
    $array = json_decode($strJsonFileContents, true);
    
    // filter unique zipcodes    
    $key = 'zipcode'; 
    $temp_array = [];
       foreach ($array as &$v) {
           if (!isset($temp_array[$v[$key]]))
           $temp_array[$v[$key]] =& $v;
       }
       $array = array_values($temp_array);

    return $array;
}

function fetch_cities() {
    $strJsonFileContents = file_get_contents("json/cities_per_provinces/nueva-ecija-cities.json");
    $locations = json_decode($strJsonFileContents, true);
    
    return $locations;
}

// 0-22
function get_nearest_citmun_per_branch($branch_key) {
 
}

// $branches = fetch_branches();
$result = fetch_cities();

echo json_encode($result);
