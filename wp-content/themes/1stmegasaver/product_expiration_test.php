<?php 

define( 'WP_USE_THEMES', false );
require(dirname(__FILE__) . '/../../../wp-load.php');

	global $wpdb;
	$today = date_i18n( 'n.d' );

	// check if there are expirable tags
	$terms = $wpdb->get_results("
		SELECT
			wp_terms.term_id,
			wp_terms.name
		FROM wp_terms
		WHERE 
			wp_terms.name LIKE '% exp'
		"	
	);
	$expired_today = false;
	foreach ($terms as $key => $value) {
		$exp_dates = str_replace(' exp', '', $value->name);
		$terms[$key]->date = $exp_dates;
		// $date1 = str_replace('.', '/', $exp_dates);
		// $date1 = date('n.d',strtotime($date1."+1 day"));
		// $terms[$key]->datee = $date1;

		if ($exp_dates == $today) {
			$expired_today = true;
			$exp_date =  $value->name;
		}
	}
// echo  json_encode($terms,JSON_PRETTY_PRINT);	
// echo $expired_today;
// echo $exp_date;
// return;

	if ($expired_today) :
	 $logs = '';	
	  $expired_products = $wpdb->get_results("
		SELECT
		    wp_posts.id as Product_ID,
			wp_posts.post_title AS Product,
			wp_posts.post_status,
			wp_terms.term_id,
			wp_terms.name
		FROM wp_posts
		LEFT JOIN wp_term_relationships
		ON wp_term_relationships.object_id = wp_posts.ID
		LEFT JOIN wp_term_taxonomy
		ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id 
		AND wp_term_taxonomy.taxonomy = 'product_tag'
		LEFT JOIN wp_terms
		ON wp_term_taxonomy.term_id = wp_terms.term_id
		WHERE wp_posts.post_type = 'product'
		AND wp_terms.name = '$exp_date'
		AND wp_posts.post_status <> 'expired'
		GROUP BY wp_posts.ID
		ORDER BY wp_posts.post_title ASC"
	);
	$expired_ids = implode("','",array_column($expired_products, 'Product_ID'));	

	echo  json_encode($expired_products,JSON_PRETTY_PRINT);	

	if($wpdb->last_error == '') {
	    $logs .= 'updated to expire status'.PHP_EOL;
	}
	else    {
		$logs .= 'error update status: '.$wpdb->print_error().PHP_EOL;
	}

	if(!empty($expired_products)) {

		$wpdb->query("
			UPDATE {$wpdb->prefix}posts 
			JOIN {$wpdb->prefix}postmeta
			ON {$wpdb->prefix}postmeta.post_id={$wpdb->prefix}posts.ID 
			SET {$wpdb->prefix}posts.post_status= 'expired' 
			WHERE {$wpdb->prefix}posts.ID IN ('".$expired_ids."')
		");
		$new_tag = $exp_date.'-d';

		// expire tag
		$q = $wpdb->query("
			UPDATE
				wp_terms
			SET
				wp_terms.name = '$new_tag'
			WHERE 
				wp_terms.name = '$exp_date'"	
		);
	}

	if($wpdb->last_error == '') {
	    $logs .= 'updated tag name';
	}
	else    {
		$logs .= 'error update tag: '.$wpdb->print_error();
	}

	//Write action to txt log
    $log  = "Date: ".date_i18n("F j, Y, g:i a").PHP_EOL.
            "Details: ".$logs.PHP_EOL.
            "-------------------------".PHP_EOL;
    //-
    file_put_contents(dirname(__FILE__) . '/../../../product-expire-logs.txt', $log, FILE_APPEND);

    else :
    	//Write action to txt log
	    $log  = "Date: ".date_i18n("F j, Y, g:i a").PHP_EOL.
	            "Details: Nothing to expire today".PHP_EOL.
	            "-------------------------".PHP_EOL;
	    //-
	    file_put_contents(dirname(__FILE__) . '/../../../product-expire-logs.txt', $log, FILE_APPEND);

	endif;
	
return;