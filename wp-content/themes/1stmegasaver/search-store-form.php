<form id="searchform">
	<div class="searchContainer">

		<input type="text" class="field search_query" placeholder="Search Store Branch by City or Province">
		<button class="clear_search" type="reset"></button>
		<button type="submit" class="search_branch_bt"><i class="fa fa-search fa-lg"></i></button>
	</div>
</form>