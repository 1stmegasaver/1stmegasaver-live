<?php
/**
 * Child theme functions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Text Domain: oceanwp
 * @link http://codex.wordpress.org/Plugin_API
 *
 */

/**
 * Load the parent style.css file
 *
 * @link http://codex.wordpress.org/Child_Themes
 */
 function megasaver_theme_enqueue_parent_style() {
	// Dynamically get version number of the parent stylesheet (lets browsers re-cache your stylesheet when you update your theme)
	$theme   = wp_get_theme( 'OceanWP' );
	$version = $theme->get( 'Version' );
		global $current_user;
		$current_user = wp_get_current_user();
		$role = ( array ) $current_user->roles;

		$css_timestamp = filemtime( get_stylesheet_directory().'/style.css' );
		define( 'THEME_VERSION', $css_timestamp );

	// Load the stylesheet
		wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'oceanwp-style' ), THEME_VERSION );

	wp_register_script( 'custom-js',  get_stylesheet_directory_uri() . '/js/megasaver.js', array( 'jquery' ), THEME_VERSION, true );
		wp_register_script( 'custom-jquery',  get_stylesheet_directory_uri() . '/js/megasaver-jquery.js', array( 'jquery' ), THEME_VERSION, true );
		wp_register_script( 'custom-checkout',  get_stylesheet_directory_uri() . '/js/megasaver-checkout.js', array( 'jquery' ), THEME_VERSION, true );
	 // wp_register_script("recaptcha_login","https://www.google.com/recaptcha/api.js");

	 $translation_array = array( 
	 	'templateUrl' => get_bloginfo('url'),
	 	'theUser'=>array(
	 			'firstname'=>$current_user->user_firstname,
	 			'role'=> $role[0])	 );
	 wp_localize_script( 'custom-js', 'cust_global', $translation_array );

	wp_enqueue_script('custom-js');
	wp_enqueue_script('custom-jquery');
	if (is_checkout())
		wp_enqueue_script('custom-checkout');
	// wp_enqueue_script('recaptcha_login');
	// wp_register_script("recaptcha_login", "https://www.google.com/recaptcha/api.js");
	
}
add_action( 'wp_enqueue_scripts', 'megasaver_theme_enqueue_parent_style' );



// CUSTOM SNIPPETS

// remove single product description title
add_filter( 'woocommerce_product_description_heading','remove_product_description_heading' );
function remove_product_description_heading() {
	return '';
}

/**
* Remove product data tabs
*/

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

		//unset( $tabs['description'] );      	// Remove the description tab
		unset( $tabs['reviews'] ); 			// Remove the reviews tab
		unset( $tabs['additional_information'] );  	// Remove the additional information tab
		unset( $tabs['brand_name'] );  	// Remove the additional information tab

		return $tabs;
}

/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {
	global $product;
	// if ($product->is_)
	$tabs['description']['title'] = __( 'Product Details' );

	return $tabs;

}

function custom_woocommerce_tag_cloud_widget() {
		$args = array(
				'number' => 5,
				'taxonomy' => 'product_tag'
		);
		return $args;
}
add_filter( 'woocommerce_product_tag_cloud_widget_args', 'custom_woocommerce_tag_cloud_widget' );

add_filter( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
function woocommerce_template_loop_product_title() {
	// if ( is_shop() && get_post_type( $id ) === 'product' ) {
	// 	return substr( $title, 0, 10 ); // change last number to the number of characters you want
	// } else {
	return substr( $title, 0, 10 );
}


//--//
add_filter('woocommerce_sale_flash', 'woocommerce_custom_sale_text', 10, 3);
function woocommerce_custom_sale_text($text, $post, $_product)
{
	global $woocommerce_loop;

		return '<div class="ribbon-holder"><div class="ribbon">SALE</div></div>';

}

add_action('woocommerce_before_shop_loop_item_title','loop_discount_price_html');
function loop_discount_price_html() {
	$save = f_show_sale_discount_price();
	if ($save > 0)
	  echo "<div class='sale-discount-price-loop'> SAVE <br>" . get_woocommerce_currency_symbol().  ' ' . number_format($save,0) . "</div>"; 
}

function single_discount_price_html() {
	$save = f_show_sale_discount_price();
	if ($save > 0)
	 	echo "<div class='sale-discount-price-single'> <span class='woocommerce-Price-amount amount'> SAVE : <span class='woocommerce-Price-currencySymbol'>₱</span>"  . number_format($save,0) . "</span></div>"; 
}

function f_show_sale_discount_price() {
		global $product; global $woocommerce;

	// if (has_term( "installment", "product_cat" )) return;

		if (!$product->is_on_sale()) return;

			if ( $product->is_type( 'simple' ) ) {
						$max_disc_price =  ( $product->get_regular_price() - $product->get_sale_price() );
			} elseif ( $product->is_type( 'variable' ) ) {
					$max_disc_price = 0;
					foreach ( $product->get_children() as $child_id ) {
						 $variation = wc_get_product( $child_id );
						 $price = $variation->get_regular_price();
						 $sale = $variation->get_sale_price();
						 if ( $price != 0 && ! empty( $sale ) ) $discount_price = ( $price - $sale );
						 if ( $discount_price > $max_disc_price ) {
								$max_disc_price = $discount_price;
						 }      
					}    
		} else {
				$max_disc_price =  ( $product->get_regular_price() - $product->get_sale_price() );
		}

		 return $max_disc_price;
}
add_shortcode('show_save_discount','single_discount_price_html');

function f_display_brand_logo($atts) {
	// get parameter(s) from the shortcode
	extract( shortcode_atts( array(
			"product-id"    => 'product-id',
	), $atts ) );

	$brands = wp_get_post_terms( $atts['product-id'], 'berocket_brand' );

	$css = is_single()? 'single':'shop-loop';
	 
	foreach( $brands as $brand ) {
		echo '<img src="'.get_bloginfo('url').'/wp-content/uploads/brands/'.strtolower($brand->name).'.jpg" class="brands-logo-'.$css.'" ondragstart="return false;">';
	}
}
add_shortcode('display_brand_logo','f_display_brand_logo');

// Display badge @ single product page
// location: woo>templates>single-product>price.php
	
add_action( 'woocommerce_before_shop_loop_item_title', 'display_brand_before_title' );
add_action( 'woocommerce_before_single_product_summary', 'display_brand_before_title' );
 function display_brand_before_title() {
	 global $product;
	 $product_id = $product->get_id();
	 do_shortcode("[display_brand_logo product-id='".$product_id."']");
}

function get_description_list($description){
	$temp = $description;

	$dom = new DOMDocument;
	$dom->loadHTML($description);

	$details = '';
	for ($i=0; $i <=3; $i++) { 
		$details .= $dom->getElementsByTagName('li')->item($i)->nodeValue;
		if ($i < 3) $details .= '<br>';
	}

	return $details;
}

function woo_social_share_under_desc() {

	// Vars
	$product_title 	= get_the_title();
	$product_url	= get_permalink();
	$product_img	= wp_get_attachment_url( get_post_thumbnail_id() ); ?>
		<span class="social-share-label">Share this:</span>
		<div class="my-social-share clr">
		<ul class="clr">
			<li class="twitter">
				<a href="https://twitter.com/intent/tweet?status=<?php echo rawurlencode( $product_title ); ?>+<?php echo esc_url( $product_url ); ?>" target="_blank" class="share-icon">
					<i class="fab fa-twitter"></i>
				</a>
			</li>
			<li class="facebook">
				<a href="https://www.facebook.com/sharer.php?u=<?php echo rawurlencode( esc_url( $product_url ) ); ?>" target="_blank"  class="share-icon">
					<i class="fab fa-facebook-f"></i>
				</a>
			</li>
			<li class="pinterest">
				<a href="https://www.pinterest.com/pin/create/button/?url=<?php echo rawurlencode( esc_url( $product_url ) ); ?>&media=<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>&description=<?php echo rawurlencode( $product_title ); ?>" target="_blank"  class="share-icon">
					<i class="fab fa-pinterest-p"></i>
				</a>
			</li>
			<li class="email">
				<a href="mailto:?subject=<?php echo rawurlencode( $product_title ); ?>&body=<?php echo esc_url( $product_url ); ?>" target="_blank"  class="share-icon">
					<i class="far fa-envelope"></i>
				</a>
			</li>
		</ul>
	</div>
<?php
}
add_action( 'woocommerce_share', 'woo_social_share_under_desc', 10 );

add_shortcode( 'facebook_page_icon', 'facebook_page_icon_f' );

//Use the email address as user_login
function oa_social_login_set_email_as_user_login ($user_fields)
{
	if ( ! empty ($user_fields['user_email']))
	{
		if ( ! username_exists ($user_fields['user_email']))
		{
			$user_fields['user_login'] = $user_fields['user_email'];
		}
	}
	return $user_fields;
}
 
//This filter is applied to new users
add_filter('oa_social_login_filter_new_user_fields', 'oa_social_login_set_email_as_user_login');


//Use a custom CSS file with Social Login
function oa_social_login_set_custom_css ($css_theme_uri)
{
 //Replace this URL by an URL to a CSS file on your own server
 $css_theme_uri = 'social-login.css';
		
 // Done
 return $css_theme_uri;
}   
add_filter('oa_social_login_default_css', 'oa_social_login_set_custom_css');
add_filter('oa_social_login_widget_css', 'oa_social_login_set_custom_css');
add_filter('oa_social_login_link_css', 'oa_social_login_set_custom_css');

// //Disable registrations using Social Login
// function oa_social_login_disable_registrations ($email)
// {
//   if ( ! email_exists ($email))
//   {
//     trigger_error ('Registrations using Social Login have been disabled', E_USER_ERROR);
//   }
//   return $email;  
// }
 
//This filter will be applied when new users are registering using social login
// add_filter('oa_social_login_filter_new_user_email', 'oa_social_login_disable_registrations');

add_filter( 'logout_url', 'pro_logout_url' );
 
function pro_logout_url( $url ) {
	 
	 $args = array( 'action' => 'logout', 'redirect_to' => apply_filters( 'wpml_home_url', get_option( 'home' ) ) );
 
	 $logout_url = add_query_arg( $args, site_url( 'wp-login.php', 'login' ) );
	 $logout_url = wp_nonce_url( $logout_url, 'log-out' );
 
	 return $logout_url;
 
}

function check_attempted_login( $user, $username, $password ) {
		if ( get_transient( 'attempted_login' ) ) {
				$datas = get_transient( 'attempted_login' );

				if ( $datas['tried'] >= 5 ) {
						$until = get_option( '_transient_timeout_' . 'attempted_login' );
						$time = time_to_go( $until );

						return new WP_Error( 'too_many_tried',  sprintf( __( '<strong>ERROR</strong>: You have reached authentication limit, you will be able to try again in %1$s.' ) , $time ) );
				}
		}

		return $user;
}

add_filter( 'authenticate', 'check_attempted_login', 30, 3 ); 
function login_failed( $username ) {
		if ( get_transient( 'attempted_login' ) ) {
				$datas = get_transient( 'attempted_login' );
				$datas['tried']++;

				if ( $datas['tried'] <= 5 )
						set_transient( 'attempted_login', $datas , 300 );
		} else {
				$datas = array(
						'tried'     => 1
				);
				set_transient( 'attempted_login', $datas , 300 );
		}
}
add_action( 'wp_login_failed', 'login_failed', 10, 1 ); 

function time_to_go($timestamp)
{

		// converting the mysql timestamp to php time
		$periods = array(
				"second",
				"minute",
				"hour",
				"day",
				"week",
				"month",
				"year"
		);
		$lengths = array(
				"60",
				"60",
				"24",
				"7",
				"4.35",
				"12"
		);
		$current_timestamp = time();
		$difference = abs($current_timestamp - $timestamp);
		for ($i = 0; $difference >= $lengths[$i] && $i < count($lengths) - 1; $i ++) {
				$difference /= $lengths[$i];
		}
		$difference = round($difference);
		if (isset($difference)) {
				if ($difference != 1)
						$periods[$i] .= "s";
						$output = "$difference $periods[$i]";
						return $output;
		}
}

	function ws_new_user_approve_autologout() {
			 if ( is_user_logged_in() ) {
								$current_user = wp_get_current_user();
								$user_id = $current_user->ID;
 
								if ( get_user_meta($user_id, 'pw_user_status', true )  === 'approved' ){ $approved = true; }
				else{ $approved = false; }
 
 
				if ( $approved ){ 
						return $redirect_url;
				}
								else{ //when user not approved yet, log them out
						wp_logout();
												return add_query_arg( 'approved', 'false', get_permalink( get_option('woocommerce_myaccount_page_id') ) );
								}
				}
}
add_action('woocommerce_registration_redirect', 'ws_new_user_approve_autologout', 99);

// BREADCRUMBS
function insert_breadcrumbs() {
	if ( !is_shop() && !is_front_page()) {
		if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
		}
	}  
}
add_action( "ocean_before_content_wrap", "insert_breadcrumbs");
add_filter( 'wpseo_breadcrumb_links', 'wpseo_breadcrumb_add_brands_link' );
function wpseo_breadcrumb_add_brands_link( $links ) {
		global $post;
		if ( is_tax('pwb-brand') || is_tax('pa_brand')  || is_tax('berocket_brand') && !is_product()) {
				$breadcrumb[] = array(
						'url' => get_permalink(925),
						'text' => 'Brands',
				);
				array_splice( $links, 1, -2, $breadcrumb );
		}
	
		return $links;
}
// end BREADCRUMBS

/**
*Assign template for search store<>branch
*/
function my_search_store_shortcode() {
	 ob_start();
	 get_template_part('search-store-form');
	 return ob_get_clean();   
} 
add_shortcode( 'my_search_store', 'my_search_store_shortcode' );  
// add_filter('woocommerce-store-notice__dismiss-link','__return_false');
/**
*Add mini sticky menu in footer mobile
*/  
// add_action('ocean_after_footer_inner', 'add_sticky_footer'); 
// function add_sticky_footer() { 
// 		echo do_shortcode('[oceanwp_library id="4685"]'); 
// }

add_action('ocean_before_footer', 'add_newsletter_footer'); 
function add_newsletter_footer() { 
		echo "<div id='newsletter_knees' style='display:none'>";
		echo do_shortcode('[mc4wp_form id="3834"]'); 
		echo "</div>";
}

// function my_account_menu_order() {
//     $menuOrder = array(
//         'orders'             => __( 'Your Orders',
//         'downloads'          => __( 'Download', 'woocommerce' ),
//         'edit-address'       => __( 'Addresses', 'woocommerce' ),
//         'edit-account'      => __( 'Account Details', 'woocommerce' ),
//         'customer-logout'    => __( 'Logout', 'woocommerce' ),
//         // 'dashboard'          => __( 'Dashboard', 'woocommerce' )
//     );
//     return $menuOrder;
//  }
//  add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );

// add_action('woocommerce_before_account_navigation', 'misha_some_content_before');
// function misha_some_content_before(){
//     global $current_user; wp_get_current_user();
//     echo  "<p id='my-account-user-profile'>Hello, ".$current_user->display_name."</p>";
// }



/**
 *Redirect to Home when shop page is accessing/requesting
 */
function custom_shop_page_redirect() {
		if( is_shop() ){
				wp_redirect( home_url() );
				exit();
		}
}
add_action( 'template_redirect', 'custom_shop_page_redirect' );

/* Login first to add to cart */
add_filter( 'woocommerce_add_to_cart_validation', 'logged_in_customers_validation', 10, 3 );
function logged_in_customers_validation( $passed, $product_id, $quantity) {
		if( ! is_user_logged_in() ) {
				$passed = false;
				
				// Displaying a custom message
				$message = __("You need to be logged in to be able adding to cart…", "woocommerce");
				$button_link = get_permalink( get_option('woocommerce_myaccount_page_id') );
				$button_text = __("Login or register", "woocommerce");
				$message .= ' <a href="'.$button_link.'" class="login-register button xoo-el-login-tgr" style="float:right;">'.$button_text.'</a>';
				
				wc_add_notice( $message, 'error' );
		}
		return $passed;
}

/**
*Remove scripts versioning
*/
function ocean_remove_script_version( $src ) {
		if ( strpos( $src, 'ver=' ) ) {
				$src = remove_query_arg( 'ver', $src );
		}
		return $src;
}
add_filter( 'script_loader_src', 'ocean_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'ocean_remove_script_version', 15, 1 );

/**
 * Add Custom Menu
 */
function register_my_menu() {
	register_nav_menu('sticky-menus',__( 'Sticky Menus' ));
}
add_action( 'init', 'register_my_menu' );

// Add custom font to font settings
function ocean_add_custom_fonts() {
		return array( 'robotoregular' ); // You can add more then 1 font to the array!
}

// DISABLE SHIPPING IN CART PAGE
function disable_shipping_calc_on_cart( $show_shipping ) {
		if( is_cart() ) {
				return false;
		}
		return $show_shipping;
}
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99 );

/**
 * Redirect Empty Cart/Checkout
 */
add_action( 'template_redirect', 'redirect_empty_cart_checkout_to_home' );
function redirect_empty_cart_checkout_to_home() {
	 if ( is_cart() && is_checkout() && 0 == WC()->cart->get_cart_contents_count() && ! is_wc_endpoint_url( 'order-pay' ) && ! is_wc_endpoint_url( 'order-received' ) ) {
			wp_safe_redirect( home_url() );
			exit;
	 }
}
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );
// CHECKOUT REMOVE SOME VALIDATION
add_filter( 'woocommerce_default_address_fields' , 'disable_address_valdiation' );
function disable_address_valdiation( $address_fields_array ) {
 
	unset( $address_fields_array['country']['validate']);
	unset( $address_fields_array['address_2']['validate']);
	// you can also hook first_name and last_name, company, country, city, address_1 and address_2
 
	return $address_fields_array;
 
}

// CHECKOUT CUSTOM VALIDATION
add_action( 'woocommerce_after_checkout_validation', 'custom_validation', 10, 2);
 
function custom_validation( $fields, $errors ){
 
		if ( preg_match( '/\\d/', $fields[ 'billing_first_name' ] ) || preg_match( '/\\d/', $fields[ 'billing_last_name' ] )  ){
				$errors->add( 'validation', 'Your first or last name contains a number. Really?' );
		}

		if(!preg_match('/^\d{11}$/', $fields['billing_phone'])) { // format 09xxxxxxxxx
			$errors->add( 'validation', 'Please enter a valid phone number' );
		}

		if(!preg_match('/^\d{4}$/', $fields['billing_postcode'])) { // format 9999
			$errors->add( 'validation', 'Please enter a valid postcode' );
		}
}

add_filter( 'woocommerce_default_address_fields', 'custom_override_default_locale_fields' );
function custom_override_default_locale_fields( $fields ) {
		$fields['state']['priority'] = 30;
		$fields['city']['priority'] = 40;
		$fields['postcode']['priority'] = 50;
		$fields['address_1']['priority'] = 60;
		return $fields;
}

add_filter("woocommerce_checkout_fields", "new_order_fields",10,1); 
function new_order_fields($fields) {
	$order = array( 
		"billing_first_name", 
		"billing_last_name", 
		"billing_state", 
		"billing_city",
		"billing_postcode", 
		"billing_address_1", 
		"billing_phone" ,
		"billing_email"
	); 

 unset($fields['billing']['billing_address_2']);

	foreach( $order as $field ) { 
		$ordered_fields[$field] = $fields["billing"][$field]; 
	}
	$fields["billing"] = $ordered_fields; 
 
	$fields["billing"]["billing_phone"]["class"][0] = 'form-row-first';
	$fields["billing"]["billing_email"]["class"][0] = 'form-row-last';
 
	// $fields["billing"]["billing_postcode"]["class"][0] = 'form-row-first';
 
	$fields['billing']['billing_address_1']['label'] = "Detailed Address";
	$fields['billing']['billing_city']['label'] = "City";

	$fields['billing']['billing_state']['label'] = "Province";
	$fields['billing']['billing_postcode']['label'] = "ZIP";

	$fields['billing']['billing_phone']['placeholder'] = '09XXXXXXXXX';

	$fields['billing']['billing_country']['default'] = 'PH';
	$fields['billing']['billing_country']['class'][0] = 'hide';

	// $fields['billing']['billing_address_1']['required'] = false;


	// make city as dropfown field
	$city_args = wp_parse_args( array(
		'type' => 'select',
		'label' => 'City',
		'options' => array(
			'select' => 'Select an option...'
		),
		'input_class' => array(
			'wc-enhanced-select',
		)
	), $fields['shipping']['shipping_city'] );
	$fields['billing']['billing_city'] = $city_args;
	wc_enqueue_js( "
	jQuery( ':input.wc-enhanced-select' ).filter( ':not(.enhanced)' ).each( function() {
		var select2_args = { minimumResultsForSearch: 5 };
		jQuery( this ).select2( select2_args ).addClass( 'enhanced' );
	});" );


	return $fields; 
}

/**
 * Display 0.00 Amount For Free Shipping Rates @ WooCommerce Cart & Checkout
 */
add_filter( 'woocommerce_cart_shipping_method_full_label', 'add_0_to_shipping_label', 10, 2 );
	 
function add_0_to_shipping_label( $label, $method ) {
	 
	// if shipping rate is 0, concatenate ": $0.00" to the label
	if ( ! ( $method->cost > 0  ) ) {
		$label .= ': ' . wc_price(0);
	} 
	 
	// return original or edited shipping label
	return $label;
	 
}


add_action( 'woocommerce_after_checkout_form', 'custom_checkout_jquery_script', 10 );
function custom_checkout_jquery_script() {
		?>
		<script type="text/javascript">
				jQuery(function($){
					// wc_checkout_params is required to continue
					if ( typeof wc_checkout_params === 'undefined' )
							return false;

				 $('select#billing_city, select#billing_state').on( 'change', function(){
						var location = $('#billing_city option:selected').html() + ' ' +  
														 $('#billing_state option:selected').html();

							console.log('Chosen area: ' + location);
							
							// Ajax: send the chosen customer location to php
							$.ajax({
									type: 'POST',
									url: wc_checkout_params.ajax_url,
									data: {
											'action': 'set_customer_area',
											'customer_area': location,
									},
									success: function (response) {
											$(document.body).trigger('update_checkout');
											// console.log('Response: '+response); // To be removed (testing)
									} 
							});
				 });
				});
				</script>
		<?php
}

// Wordpress Ajax: Saved the selected customer location to WC_Session
add_action( 'wp_ajax_nopriv_set_customer_area', 'set_customer_area_in_wc_sessions' );
add_action( 'wp_ajax_set_customer_area', 'set_customer_area_in_wc_sessions' );
function set_customer_area_in_wc_sessions() {
		if( ! isset($_POST['customer_area']) ) return;

		// Encoding the customer's location for Google API
		// $customer_area_ena = rawurlencode( $_POST['customer_area'] );

		// Set the chosen customer location in WC_Sessions
		WC()->session->set('customer_area', rawurlencode($_POST['customer_area']) );

		// To be removed (testing: Send back the data to jQuery)
		// echo json_encode( WC()->session->get('customer_area' ) );

		die(); // To avoid server error 500
}


add_filter( 'woocommerce_package_rates', 'distance_shipping_calculated_surcharge', 9999, 2 );
function distance_shipping_calculated_surcharge( $rates, $package ) {
		$customer_area = WC()->session->get('customer_area' );
		$customer_coordinates = '';
		// var_dump($package);
	 // if(!isset($_POST['customer_area'])) return;

		// hide
		//shipping_method_0_free_shipping16 
		// free_shipping:16
		// shipping_method_0_flat_rate7
		// shipping_method_0_local_pickup13

		// if (!isset($customer_area))
		//   return;

		foreach($rates as $rate_key => $rate_values) {
				$method_id = $rate_values->method_id;
				$rate_id = $rate_values->id;
				
				// echo $method_id;

				if ( 'flat_rate' === $rate_values->method_id ) {
			// if ( isset( $rates['flat_rate:7'] ) ) {
					$url = get_bloginfo('url').'/wp-content/themes/1stmegasaver/googleapis.php?address='.$customer_area;
						// echo $url;
						
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

						$output = json_decode(curl_exec($ch),true);
						curl_close($ch);

						// var_dump($output);

						// calculate new shipping fee
						// subject to change
						$distance = floatval(ceil($output['distance(km)']));
						$minimum = 45;
						// $standard = floatval($rates[$rate_id]->cost);
						$standard = 5;
						if ($distance <= 5 )
							$new_fee = $minimum;
						// else if ($distance > 5 && $distance <=10)
						else
							$new_fee = $minimum + ($standard * ($distance - 5 ));
						// else
							// $new_fee = 450;   
						// $new_fee = floatval($rates[$rate_id]->cost) * floatval($distance);
						// $rates['flat_rate:7']->cost = number_format($new_fee,2);
						$rates[$rate_id]->cost = number_format($new_fee,2);
			// }      
				}
		}
		return $rates;
}    

/**
 * Add Order Note @ Checkout Page
 */
 
// add_filter( 'woocommerce_review_order_after_shipping', 'notice_shipping' ,10);
 
// function notice_shipping() {
// // echo '<div class="shipping-notice">
//     // Please allow 7-15 business days for delivery after order processing.</div>';
// }


//  change shipping message
add_filter('woocommerce_shipping_may_be_available_html','new_message');
function new_message($message) {
	return "Enter your complete address to check shipping options.";
}

/**
 *CHECKOUT - Add Inline Field Error Notifications */
 
add_filter( 'woocommerce_form_field', 'checkout_fields_in_label_error', 10, 4 );
 
function checkout_fields_in_label_error( $field, $key, $args, $value ) {
	 if ( strpos( $field, '</label>' ) !== false && $args['required'] ) {
			$error = '<span class="error" style="display:none">';
			$error .= sprintf( __( '%s is a required field.', 'woocommerce' ), $args['label'] );
			$error .= '</span>';
			$field = substr_replace( $field, $error, strpos( $field, '</label>' ), 0);
	 }
	 return $field;
}

// CHECKOUT DEFAULT COUNTRY (backend) - PH 
add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );
function change_default_checkout_country() {
	return 'PH'; 
}

add_filter( 'woocommerce_states', 'custom_woocommerce_states' );
function custom_woocommerce_states( $states ) {
	$states['PH'] = array(
		'BAN' => 'Bataan',
		'TAR' => 'Tarlac',
		'BEN' => 'Benguet',
		'BUL' => 'Bulacan',
		'CAG' => 'Cagayan',
		'ILS' => 'Ilocos Sur',
		'ISA' => 'Isabela',
		'LUN' => 'La Union',
		'NUE' => 'Nueva Ecija',
		'NUV' => 'Nueva Vizcaya',
		'PAM' => 'Pampanga',
		'PAN' => 'Pangasinan',
		'QUI' => 'Quirino',
		'00'  => 'Metro Manila'
	);
	return $states;
}

function branches() {
		global $wpdb;

		$results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}branches WHERE id = 1", ARRAY_N);
		return $results;
}

function google_api_options() {
	add_menu_page("Google Api", "Google Api", "manage_options", "google-api-options", "google_options_page", "", 100);
}
function google_options_page() { ?>
	<div class="wrap">
	<h1>Google Api Key's</h1>
	<form method="post" action="options.php">
	<?php settings_fields("header_section");
	do_settings_sections("google-api-options");
	submit_button(); ?>  
	</form>
	</div>
<?php }

add_action("admin_menu", "google_api_options");
function google_api_settings() {
	add_settings_section("header_section", "", "", "google-api-options");
	add_settings_field("geocoding_api_key", __("Geocoding Key"), "geocoding_api_key_element", "google-api-options", "header_section");
	add_settings_field("distance_api_key", __("Distance Matrix Key"), "distance_api_key_element", "google-api-options", "header_section");
	register_setting("header_section", "geocoding_api_key");
	register_setting("header_section", "distance_api_key");
}


function geocoding_api_key_element() { ?>
	<input type="text" name="geocoding_api_key" id="geocoding_api_key" value="<?php echo get_option('geocoding_api_key'); ?>" style="width:400px"/>
<?php }

function distance_api_key_element() { ?>
	<input type="text" name="distance_api_key" id="distance_api_key" value="<?php echo get_option('distance_api_key'); ?>" style="width:400px"/>
<?php }


add_action("admin_init", "google_api_settings");

/**
 * Display the monthly installment
 */
function cfwc_create_custom_field() {
 $args = array(
 'id' => '_monthly_installment',
 'label' => __( 'Monthly Installment (₱)', 'short' ),
 'class' => 'short wc_input_price',
 'wrapper_class'=> 'woocommerce-Price-amount amount',
 'desc_tip' => false
 );
 wc_format_localized_price( $args['value'] );
 woocommerce_wp_text_input( $args );

 $args = array(
 'id' => '_terms_installment',
 'label' => __( 'Installment Terms (Months: 3,6,9,12)', 'short' ),
 'class' => 'short wc_input_',
 'wrapper_class'=> '',
 'desc_tip' => false,
 );
 wc_format_localized_price( $args['value'] );
 woocommerce_wp_text_input( $args );

  $args = array(
 'id' => 'product_note',
 'label' => __( 'Product Note', 'cfwc' ),
 'class' => 'product-note',
 'desc_tip' => false
 );
 woocommerce_wp_text_input( $args );

}
add_action( 'woocommerce_product_options_general_product_data', 'cfwc_create_custom_field' );

/**
 * Save monthly installment in database
 */
function cfwc_save_custom_field( $post_id ) {
 $product = wc_get_product( $post_id );
 $title = isset( $_POST['_monthly_installment'] ) ? $_POST['_monthly_installment'] : '';
 $terms = isset( $_POST['_terms_installment'] ) ? $_POST['_terms_installment'] : '';
 $note = isset( $_POST['product_note'] ) ? $_POST['product_note'] : '';
 $product->update_meta_data( '_monthly_installment', sanitize_text_field( $title ) );
 $product->update_meta_data( '_terms_installment', sanitize_text_field( $terms ) );
 $product->update_meta_data( 'product_note', sanitize_text_field( $note ) );

 $product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field' );

/**
 * Display monthly installment on the front end
 */
function cfwc_display_custom_field() {
 global $post;
 global $woocommerce;
 // Check for the custom field value
 $product = wc_get_product( $post->ID );
 $title = $product->get_meta( '_monthly_installment' );
 $terms =  $product->get_meta( '_terms_installment' );
 if ($terms) {
	$term = $terms;
 } else {
	 if (has_term('installment','product_cat')) {
		$terms = 12;
	 }
	 else if (has_term('appliances','product_cat')) {
		$terms = 12;
	 }
	 else if (has_term('mobile','product_cat') || has_term('gadgets','product_cat')) {
		$terms = 6;
	}
	 else if (has_term('furniture','product_cat')) {
		$terms = 9;
	 }
	 else{}
}

 if( $title ) {
 // Only display our field if we've got a value for the field title
	// if (!has_term('installment','product_cat')) return;

	echo get_woocommerce_currency_symbol() . number_format($title,0).' / '.$terms.'months';
 } 
}
add_shortcode('monthly_installment','cfwc_display_custom_field');

/**
 * Display product note
 */
function display_note_f() {
 global $post;
 // Check for the custom field value
 $product = wc_get_product( $post->ID );
 $note = $product->get_meta( 'product_note' );
 if( $note ) {
 // Only display our field if we've got a value for the field title
 printf(
 '<div class="product-note text-danger">%s</div>',
 esc_html( $note )
 );
 }
}
add_shortcode( 'display_note', 'display_note_f' );

function show_installment_amount($product) {
		// HERE your custom button text and link
		$button_text = __( "APPLY NOW", "woocommerce" );
		# $button_link = get_bloginfo('url').'/1st-megasaver-installment-application-form/';
		$button_link = get_bloginfo('url').'/easy-in-house-installment/';

		// Display button
		echo "<p id='easy-inhouse-wrapper'>";
		// if (has_term('gadgets','product_cat') || has_term('mobile','product_cat'))
		// 	echo "<img src='".get_bloginfo('url')."/wp-content/uploads/2019/05/easy-1-64x20.png'>";
		// else {
			echo "<img src='".get_bloginfo('url')."/wp-content/uploads/2019/05/easy-1-64x20.png'>"; 
		    cfwc_display_custom_field();
			echo '&emsp;<a class="single_apply_now" href="'.$button_link.'" target="_blank">' . $button_text . '</a>';
		// }
		echo "</p>";
}

add_action('woocommerce_after_add_to_cart_form','show_installment_amount');

/**
 * Remove all Google font options from the Customizer
 */
function remove_customizer_google_fonts( $fonts ) {
	if ( is_customize_preview() ) {
		$fonts = '';
	}
	return $fonts;
}
add_filter( 'ocean_google_fonts_array', 'remove_customizer_google_fonts' );

//  PRODUCT EXPIRATION

function update_expiry_products(){
	$url = get_bloginfo('url').'/wp-content/themes/1stmegasaver/product_expiration_test.php';
						
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$output = json_decode(curl_exec($ch),true);
	curl_close($ch);

}
add_action( 'product_expire', 'update_expiry_products' );

// Registering custom post status
function wpb_custom_post_status(){
    register_post_status('expired', array(
        'label'                     => _x( 'Expired', 'product' ),
        'public'                    => false,
        'private'					=> true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'post_type'                 => array( 'product'),
        'label_count'               => _n_noop( 'Expired <span class="count">(%s)</span>', 'Expired <span class="count">(%s)</span>' ),
    ) );
}
add_action( 'init', 'wpb_custom_post_status' ,9);

// Using jQuery to add it to post status dropdown
add_action('admin_footer-post.php', 'wpb_append_post_status_list');
function wpb_append_post_status_list(){
	global $post;
	$complete = '';
	$label = '';
	if($post->post_type == 'product' && $post->post_status == 'expired') { 
		if($post->post_status == 'expired'){
			$complete = ' selected="selected"';
			$label = '<span id="post-status-display"> Expired</span>';

			echo '
			<script>
			jQuery(document).ready(function($){
			$("select#post_status").append(\'<option value="expired" '.$complete.'>Expired</option>\');
			$("span#post-status-display").html("Expired");
			});
			</script>
			'; 

		}
	
	}
}

function kc_add_taxonomy_filters() {
global $typenow;

// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
$my_taxonomies = array(  'product_tag' );
switch($typenow){

    case 'product':

        foreach ($my_taxonomies as $tax_slug) {


                    $tax_obj = get_taxonomy($tax_slug);
                    $tax_name = $tax_obj->labels->name;
                    $terms = get_terms($tax_slug);
                    if(count($terms) > 0) {
                        echo "<select name='tag' id='$tax_slug' class='postform alignleft actions'>";
                        echo "<option value=''>Show All $tax_name</option>";
                        foreach ($terms as $term) {
                            echo '<option value="', $term->slug,'" ',selected( @$_GET[$tax_slug] == $term->slug , $current = true, $echo = false ) , '>' , $term->name ,' (' , $term->count ,')</option>';
                        }
                        echo "</select>";
                    }

        }


    break;
}
}  

add_action( 'restrict_manage_posts', 'kc_add_taxonomy_filters' );



// WARNING: KEEP THIS FUNCTION TO END OF THIS FILE
function hide_something_to_users() {

}
add_action('init','hide_something_to_users');
