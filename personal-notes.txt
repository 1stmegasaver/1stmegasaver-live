1st Megasaver Online Store
1stmegasaver_admin
1stmegasaver_2019
1stmegasaver.mis@gmail.com

1stmegasaver.mis@gmail.com
password: admin.1234

===========================================================

UPDATE wp_options
SET option_value = 'http://new-domain-name.com'
WHERE option_name = 'home';

UPDATE wp_options
SET option_value = 'http://new-domain-name.com'
WHERE option_name = 'siteurl';

UPDATE wp_posts
SET post_content = REPLACE(post_content,'http://localhost/1stmegasaver','http://new-domain-name.com');

UPDATE wp_posts
SET guid = REPLACE(guid,'http://localhost/1stmegasaver','http://new-domain-name.com');